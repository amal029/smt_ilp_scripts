#!/usr/bin/env python

import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn import tree
from sklearn.cross_validation import cross_val_score
import statsmodels.formula.api as smf
from sklearn.metrics import accuracy_score
from sklearn.cross_validation import KFold
import numpy as np
import matplotlib.pyplot as plt


def logit(resp='', data=None, preds=None):
    query = resp + ' ~ 1 + ' + ' + '.join(preds)
    return smf.logit(formula=query, data=data).fit()


def main():
    # Read the file
    data = pd.read_csv('results_unique_column.csv',
                       names=['Instance', 'Structure',
                              'Balanced', 'Ignore1',
                              'Tasks', 'CCR',
                              'Random', 'Procs',
                              'Technique', 'Milptech',
                              'Symmetry', 'Lowerbound',
                              'Solution', 'Time'],
                       delimiter=',')

    # This is the response variable
    data['Time'][data['Time'] <= 60.0] = 1  # Could be solved
    data['Time'][data['Time'] > 60.0] = 0   # Could not be solved

    # le3 = LabelEncoder()
    # le3.fit(data['Time'])
    # data['Time'] = le3.transform(data['Time'])
    # print data['Time'].describe()

    # Drop the indpendent values
    data['CCR'][data['CCR'] == "None"] = -1
    # Round the CCR values
    data['CCR'] = data['CCR'].astype(float).round()

    # data['CCR'][data['CCR'] == -1] = 'NONE'
    # data['CCR'][data['CCR'] == 0] = 'LOW'
    # data['CCR'][data['CCR'] == 1] = 'MID'
    # data['CCR'][data['CCR'] == 10] = 'HIGH'

    data1 = data.copy()         # This has to be here!

    # Convert the CCR values to sklearn's classifier type
    le1 = LabelEncoder()
    le1.fit(data['CCR'])
    data['CCR'] = le1.transform(data['CCR'])

    # Now convert the structures
    p = data['Structure'].unique()
    le2 = LabelEncoder()
    le2.fit(data['Structure'])
    data['Structure'] = le2.transform(data['Structure'])
    pdict = {}
    for k, v in zip(p, le2.transform(p)):
        pdict.update({k: v})

    smt_data = data[data['Technique'] == 'smt']
    smt_data = smt_data.drop(['Solution', 'Instance', 'Lowerbound',
                              'Balanced', 'Ignore1', 'Milptech',
                              'Symmetry', 'Random', 'Technique'], axis=1)

    # Now build the decision_tree for smt
    smt_dt_clf = DecisionTreeClassifier()
    exog = smt_data.drop('Time', axis=1)
    endog = smt_data['Time']
    smt_dt_clf.fit(exog, endog)

    # Now do random forests for smt
    smt_rf_clf = RandomForestClassifier(n_jobs=-1)
    smt_rf_clf.fit(exog, endog)

    # print 'SMT LOGIT:'
    smt_data = data1[data1['Technique'] == 'smt']
    smt_data = smt_data.drop(['Solution', 'Instance', 'Lowerbound',
                              'Balanced', 'Ignore1', 'Milptech',
                              'Symmetry', 'Random', 'Technique'], axis=1)
    smt_preds = [x for x in smt_data.columns.values if x != 'Time']
    smt_preds = ['C(' + x + ')' if x == 'Structure' else x for x in smt_preds]
    # smt_preds = ['C(' + x + ')' if x == 'CCR' else x for x in smt_preds]
    smt_preds = ['C(' + x + ')' if x == 'Procs' else x for x in smt_preds]

    # Let us compare the different techniques in terms of
    # corss_validation on the rss
    smt_dt_scores = cross_val_score(smt_dt_clf, cv=10,
                                    n_jobs=1, X=exog, y=endog,
                                    scoring='accuracy')
    smt_rf_scores = cross_val_score(smt_rf_clf, cv=10,
                                    n_jobs=1, X=exog, y=endog,
                                    scoring='accuracy')

    smt_logit_scores = pd.DataFrame({'scores': [np.nan]*10})
    # Remove Structure 3, 4 and CCR 3
    # smt_data = smt_data[smt_data['Structure'] != 'Independent']
    # smt_data = smt_data[smt_data['Structure'] != 'Join']
    # smt_data = smt_data[smt_data['CCR'] != 'None']
    # Now we need to do the cross_validation explicitly
    kf = KFold(len(smt_data), n_folds=10)
    smt_logit = None
    for i, (train_index, test_index) in zip(xrange(10), kf):
        # First predict the values
        smt_logit = logit(resp='Time',
                          data=smt_data.iloc[train_index],
                          preds=smt_preds)
        predicted = smt_logit.predict(smt_data.iloc[test_index])
        actual = endog.iloc[test_index]
        smt_logit_scores.iloc[i] = accuracy_score(actual, predicted.round())

    # The final result comparing the accuracy of all the three different
    # methods.
    print (smt_dt_scores.mean(), ':', smt_dt_scores.std(),
           smt_rf_scores.mean(), ':', smt_rf_scores.std(),
           smt_logit_scores.mean()[0], ':', smt_logit_scores.std()[0])

    print "SMT decision_tree:"
    print smt_dt_clf.feature_importances_, exog.columns.values
    tree.export_graphviz(smt_dt_clf.tree_,
                         out_file='smt_dt_tree.dot',
                         filled=True, feature_names=exog.columns.values,
                         class_names=['No', 'Yes'])

    plt.style.use('ggplot')
    figure, ax = plt.subplots(2, 2)
    ff, (a, b) = plt.subplots(1, 2)

    print "SMT random forest:"
    print smt_rf_clf.feature_importances_, exog.columns.values
    ax[0][0].pie(smt_dt_clf.feature_importances_, labels=exog.columns.values,
                 autopct='%1.1f%%')
    ax[0][0].set_title('SMT decision tree influence')
    a.set_title('SMT')
    a.set_aspect('equal')
    a.pie(smt_dt_clf.feature_importances_, labels=exog.columns.values,
          autopct='%1.1f%%')

    print "SMT Logistic regression:"
    print smt_logit.summary()

    # Now do the same thing from the ilp data set
    ilp_data = data[data['Technique'] == 'milp']

    ilp_data = ilp_data.drop(['Solution', 'Instance', 'Lowerbound',
                              'Balanced', 'Ignore1', 'Random',
                              'Technique'], axis=1)
    le4 = LabelEncoder()
    # mp = ilp_data['Milptech'].unique()
    ilp_data['Milptech'] = le4.fit_transform(ilp_data['Milptech'])

    le5 = LabelEncoder()
    # sp = ilp_data['Symmetry'].unique()
    ilp_data['Symmetry'] = le5.fit_transform(ilp_data['Symmetry'])

    # mpd = {}
    # for k, v in zip(mp, le4.transform(mp)):
    #     mpd.update({k: v})

    # spd = {}
    # for k, v in zip(sp, le5.transform(sp)):
    #     spd.update({k: v})

    exog = ilp_data.drop(['Time'], axis=1)
    endog = ilp_data['Time']
    ilp_dt_clf = DecisionTreeClassifier()
    ilp_dt_clf.fit(exog, endog)

    ilp_rf_clf = RandomForestClassifier(n_jobs=-1)
    ilp_rf_clf.fit(exog, endog)

    # Let us compare the different techniques in terms of
    # corss_validation on the rss
    ilp_dt_scores = cross_val_score(ilp_dt_clf, cv=10,
                                    n_jobs=1, X=exog, y=endog,
                                    scoring='accuracy')
    ilp_rf_scores = cross_val_score(ilp_rf_clf, cv=10,
                                    n_jobs=1, X=exog, y=endog,
                                    scoring='accuracy')

    # DecisionTreeClassifier/RandomForestClassifier without Milptech and
    # Symmetry
    exog1 = exog.drop(['Milptech'], axis=1)
    ilp_dt_clf1 = DecisionTreeClassifier().fit(exog1, endog)
    ilp_rf_clf1 = RandomForestClassifier(n_jobs=-1).fit(exog1, endog)

    # Let us compare the different techniques in terms of
    # corss_validation on the rss
    ilp_dt_scores1 = cross_val_score(ilp_dt_clf1, cv=10,
                                     n_jobs=1, X=exog1, y=endog,
                                     scoring='accuracy')
    ilp_rf_scores1 = cross_val_score(ilp_rf_clf1, cv=10,
                                     n_jobs=1, X=exog1, y=endog,
                                     scoring='accuracy')

    # DecisionTreeClassifier/RandomForestClassifier without Milptech and
    # Symmetry
    exog2 = exog.drop(['Milptech', 'Symmetry'], axis=1)
    ilp_dt_clf2 = DecisionTreeClassifier().fit(exog2, endog)
    ilp_rf_clf2 = RandomForestClassifier(n_jobs=-1).fit(exog2, endog)

    # Let us compare the different techniques in terms of
    # corss_validation on the rss
    ilp_dt_scores2 = cross_val_score(ilp_dt_clf2, cv=10,
                                     n_jobs=1, X=exog2, y=endog,
                                     scoring='accuracy')
    ilp_rf_scores2 = cross_val_score(ilp_rf_clf2, cv=10,
                                     n_jobs=1, X=exog2, y=endog,
                                     scoring='accuracy')

    # Now let us do the logit for this guy
    ilp_data = data1[data1['Technique'] == 'milp']
    ilp_data = ilp_data.drop(['Solution', 'Instance', 'Lowerbound',
                              'Balanced', 'Ignore1', 'Random',
                              'Technique', 'Milptech'], axis=1)

    ilp_logit_scores = pd.DataFrame({'scores': [np.nan]*10})

    ilp_preds = [x for x in ilp_data.columns.values if x != 'Time']
    ilp_preds = ['C(' + x + ')' if x == 'Structure' else x for x in ilp_preds]
    # ilp_preds = ['C(' + x + ')' if x == 'CCR' else x for x in ilp_preds]
    ilp_preds = ['C(' + x + ')' if x == 'Procs' else x for x in smt_preds]

    # ilp_preds = ['C(' + x + ')' if x == 'Milptech' else x for x in ilp_preds]
    ilp_preds = ['C(' + x + ')' if x == 'Symmetry' else x for x in ilp_preds]
    # ilp_data = ilp_data[ilp_data['Structure'] != 'Independent']
    # ilp_data = ilp_data[ilp_data['Structure'] != 'Join']
    # Now we need to do the cross_validation explicitly
    kf = KFold(len(ilp_data), n_folds=10)
    for i, (train_index, test_index) in zip(xrange(10), kf):
        # First predict the values
        ilp_logit = logit(resp='Time',
                          data=ilp_data.iloc[train_index],
                          preds=ilp_preds)
        # print ilp_logit.summary()
        predicted = ilp_logit.predict(ilp_data.iloc[test_index])
        actual = endog.iloc[test_index]
        ilp_logit_scores.iloc[i] = accuracy_score(actual, predicted.round())

    print (ilp_dt_scores.mean(), ':', ilp_dt_scores.std(),
           ilp_rf_scores.mean(), ':', ilp_rf_scores.std(),
           ilp_logit_scores.mean()[0], ':', ilp_logit_scores.std()[0])

    print 'ILP without Milptech'
    print ilp_dt_scores1.mean(), ':', ilp_dt_scores1.std()
    print ilp_rf_scores1.mean(), ':', ilp_rf_scores1.std()

    print 'ILP without Milptech and Symmetry'
    print ilp_dt_scores2.mean(), ':', ilp_dt_scores2.std()
    print ilp_rf_scores2.mean(), ':', ilp_rf_scores2.std()

    # print 'ILP decision_tree:'
    # print ilp_dt_clf.feature_importances_, exog.columns.values

    ax[0][1].pie(ilp_dt_clf.feature_importances_, labels=exog.columns.values,
                 autopct='%1.1f%%')
    ax[0][1].set_title('ILP decision tree influence')
    ax[1][0].pie(ilp_dt_clf1.feature_importances_, labels=exog1.columns.values,
                 autopct='%1.1f%%')
    ax[1][0].set_title('ILP DT\{Symmetry}')
    ax[1][1].pie(ilp_dt_clf2.feature_importances_, labels=exog2.columns.values,
                 autopct='%1.1f%%')
    ax[1][1].set_title('ILP DT\{Milptech, Symmetry}')
    b.set_aspect('equal')
    b.pie(ilp_dt_clf2.feature_importances_, labels=exog2.columns.values,
          autopct='%1.1f%%')
    b.set_title('MILP')
    ff.savefig('../../../smt-ilp/figures/influence.pdf',
               bbox_inches='tight',
               transparent=True,
               pad_inches=0)

    tree.export_graphviz(ilp_dt_clf.tree_,
                         out_file='ilp_dt_tree.dot',
                         filled=True, feature_names=exog.columns.values,
                         class_names=['No', 'Yes'])

    print 'ILP random forest:'
    print ilp_rf_clf.feature_importances_, exog.columns.values

    print 'ILP logistic regression:'
    print ilp_logit.summary()

    print 'Structure dict:', pdict

    plt.show()

if __name__ == '__main__':
    main()
