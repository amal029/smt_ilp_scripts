import pydot

import os

from smt_formulate import calc_makespan_with_smt

import sys

USE_FULL_DIR = True

if not USE_FULL_DIR:
  sys.path.append(".")
  from two_procs_files import my_files

for dirname, dirnames, filenames in os.walk('.'):

    # print path to all filenames.
    if USE_FULL_DIR:
      for filename in filenames:
          fullname = os.path.join(dirname, filename)
#          print fullname[-4:]
          if fullname[-4:] != ".dot":
            continue

          print "Processing", fullname

#          makespan, takes = calc_makespan_with_smt(fullname, 2, verbose=False, progress=True, timed=True)
          makespan, takes = calc_makespan_with_smt(fullname, 4, verbose=False, progress=True, timed=True)

          print "RESULT, ", fullname, ",", makespan, ",", takes
    else:
      for filename in my_files:
          fullname = os.path.join(dirname, filename + ".dot")

          print "Processing", fullname

          makespan, takes = calc_makespan_with_smt(fullname, 2, verbose=False, progress=True, timed=True)

          print "RESULT, ", fullname, ",", makespan, ",", takes
