#!/usr/bin/env python

import csv


# The unique ones are columns 1,4,5, and 7

def read_csv(file='results.csv'):
    with open(file, 'rb') as cf:
        ret = set()
        reader = csv.reader(cf)
        try:
            for r in reader:
                if r[5] != 'None':
                    ret.add('_'.join([r[1], r[4],
                                      str(round(float(r[5]))), r[7]]))
                else:
                    ret.add('_'.join([r[1], r[4], r[5], r[7]]))
        except Exception as e:
            print e, r
        return {x: i for i, x in enumerate(ret)}


# Now make the unique column
def update_csv(file='results.csv', rdict=None,
               output_file='output.csv'):
    ll = list()
    with open(file, 'rb') as cf:
        reader = csv.reader(cf)
        for r in reader:
            if r[5] != 'None':
                r[0] = rdict['_'.join([r[1], r[4],
                                       str(round(float(r[5]))), r[7]])]
            else:
                r[0] = rdict[('_'.join([r[1], r[4], r[5], r[7]]))]
            ll += [r]
    # Write the csv file back
    with open(output_file, 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(ll)

update_csv(rdict=read_csv(), output_file='results_unique_column.csv')
if __name__ == '__main__':
    update_csv(rdict=read_csv(), output_file='results_unique_column.csv')
