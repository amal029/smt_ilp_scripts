#!/usr/bin/env python

import os

def my_files1(tt=None, ccr=None, convert=False):
    thefiles = []

    for dirname, dirnames, filenames in os.walk('.'):
        for filename in filenames:
            fullname = os.path.join(dirname, filename)
            if "/DAG" not in fullname:
                continue
            if (fullname[-4:] != ".gxl"):
                continue
            newname = fullname[:-4] + '.dot'
            if convert:
              convertStr = 'gxl2dot -o"' + newname + '" "' + fullname + '"'
              print 'Executing "' + convertStr + '"'
              os.system(convertStr)
            if tt is not None and ('/'+tt+"/DAG") in fullname and ccr is not None and ("/"+ccr) in fullname:
                thefiles.append(newname)
            if tt is not None and ('/'+tt+"/DAG") in fullname and ccr is None:
                thefiles.append(newname)

    return thefiles

def my_files2(convert=False):
  return ['./data/DAG1/In Tree/Nodes 21/CCR 0.1/Random/InTree-Balanced-MaxBf-3_Nodes_21_CCR_0.10_WeightType_Random.dot']

def my_files(tt=None, ccr=None, convert=False):
  return my_files1(tt, ccr, convert)
#  return my_files2(convert)

import csv
done = []
    
def get_done():
    global done

#  with open("files_done.csv", "rb") as f:
    try:
        with open("results.csv", "rb") as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                done.append(','.join(str(x) for x in row))
    except IOError:
        pass

def is_done(dir, name, chars, procs, alg, subalg, extra):

  strList = [dir, name, chars['spread'], chars['MaxBf'], chars['Nodes'], chars['CCR'], chars['WeightType'],
             procs, alg, subalg, extra]

  start = ','.join(str(x) for x in strList)

  for r in done:
    if r.startswith(start):
      return True

  return False

if __name__ == "__main__":
  files = my_files(convert=True)
#  print files

