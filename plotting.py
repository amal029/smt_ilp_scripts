#!/usr/bin/env python

from __future__ import division
import csv
import matplotlib.pyplot as plt
import colorsys
import argparse


def plot_instances(u_data, smt_data, basic_sym, basic_asym,
                   rel_sym, rel_asym, red_sym, red_asym, X, usmt,
                   inst):
    fig, ax = plt.subplots()
    width = 0.35
    ax.bar(X, u_data, width=width, color='black', label='unsolved')
    ax.bar([x + width for x in X], smt_data, color='green', width=width,
           label='smt')
    ax.bar([x + 2*width for x in X], [basic_sym], color='red', width=width,
           label='basic_sym')
    ax.bar([x + 3*width for x in X], [basic_asym], color='maroon',
           width=width, label='basic_asym')
    ax.bar([x + 4*width for x in X], [red_sym], color='blue',
           width=width, label='ilp_red_sym')
    ax.bar([x + 5*width for x in X], [red_asym], color='pink',
           width=width, label='ilp_red_asym')
    ax.bar([x + 6*width for x in X], [rel_sym], color='yellow',
           width=width, label='ilp_rel_sym')
    ax.bar([x + 7*width for x in X], [rel_asym], color='gray',
           width=width, label='ilp_rel_asym')

    ax.set_ylabel('% contribution by different Techniques')
    ax.set_title('Comparison of different solving techniques')
    ax.set_xlabel(list(inst)[0])
    plt.legend(bbox_to_anchor=(1.1, 1.07), loc=2)
    plt.grid(which='major')
    plt.grid(which='minor')
    plt.savefig(list(inst)[0]+'.pdf', format='pdf', bbox_inches='tight')


# Read the 'CSV' file
def readFile(name):
    milps = []
    smts = []
    with open(name, 'rb') as csvfile:
        reader = csv.DictReader(csvfile,
                                ['Instance', 'Structure',
                                 'Balanced', 'Ignore1',
                                 'Tasks', 'CCR',
                                 'Random', 'Procs',
                                 'Technique', 'Milptech',
                                 'Symmetry', 'Lowerbound',
                                 'Solution', 'Time'])

        # Make a list of dicts
        maxInst = 0
        structures = []
        ccrs = []
        procs = []
        tasks = []
        for r in reader:
            if r['CCR'] == 'None':
                r['CCR'] = str(-1)
            else:
                tmp = str(round(float(r['CCR'])))
                if tmp == '0.0':
                    r['CCR'] = '0.1'
                else:
                    r['CCR'] = str(round(float(r['CCR'])))

            structures += [r['Structure']]
            ccrs += [r['CCR']]
            procs += [r['Procs']]
            tasks += [r['Tasks']]
            maxInst = max(int(r['Instance']), maxInst)
            if r['Technique'] == 'smt':
                smts += [r]
            else:
                milps += [r]

        return (smts, milps, maxInst, list(set(structures)),
                list(set(ccrs)), list(set(procs)),
                list(set(tasks)))


def solved_instances(smts, milps, largestInst, plot, debug=True):
    s_smts = []
    s_basics_asym = []
    s_basics_sym = []
    s_rel_sym = []
    s_rel_asym = []
    s_red_sym = []
    s_red_asym = []
    s_unsolved = []
    for i in xrange(largestInst+1):
        inst = set([x['Structure']+'_CCR_'+x['CCR'] +
                    '_PROCS_' + x['Procs'] + '_#TASKS_' +
                    x['Tasks'] + '_INST_' +
                    x['Instance']
                    for x in smts
                    if int(x['Instance']) == i])
        solved_smts = [x
                       for x in smts
                       if float(x['Time']) < 60 and
                       int(x['Instance']) == i]
        s_smts += solved_smts
        solved_basics_sym = [x for x in milps
                             if (x['Milptech'] == 'BASIC' and
                                 float(x['Time']) < 60 and
                                 x['Symmetry'] == 'no antisym' and
                                 int(x['Instance']) == i)]
        s_basics_sym += solved_basics_sym
        solved_basics_asym = [x for x in milps
                              if (x['Milptech'] == 'BASIC' and
                                  float(x['Time']) < 60 and
                                  x['Symmetry'] == 'antisym' and
                                  int(x['Instance']) == i)]
        s_basics_asym += solved_basics_asym
        solved_relaxed_sym = [x for x in milps
                              if (x['Milptech'] == 'RELAXED' and
                                  float(x['Time']) < 60 and
                                  x['Symmetry'] == 'no antisym'and
                                  int(x['Instance']) == i)]
        s_rel_sym += solved_relaxed_sym
        solved_relaxed_asym = [x for x in milps
                               if (x['Milptech'] == 'RELAXED' and
                                   float(x['Time']) < 60 and
                                   x['Symmetry'] == 'antisym' and
                                   int(x['Instance']) == i)]
        s_rel_asym += solved_relaxed_asym
        solved_reduced_sym = [x for x in milps
                              if (x['Milptech'] == 'REDUCED' and
                                  float(x['Time']) < 60 and
                                  x['Symmetry'] == 'no antisym' and
                                  int(x['Instance']) == i)]
        s_red_sym += solved_reduced_sym
        solved_reduced_asym = [x for x in milps
                               if (x['Milptech'] == 'REDUCED' and
                                   float(x['Time']) < 60 and
                                   x['Symmetry'] == 'antisym' and
                                   int(x['Instance']) == i)]
        s_red_asym += solved_reduced_asym
        unsolved1 = [x
                     for x in smts
                     if float(x['Time']) >= 60 and
                     int(x['Instance']) == i]
        unsolved2 = [x
                     for x in milps
                     if float(x['Time']) >= 60 and
                     int(x['Instance']) == i]
        unsolved = unsolved1 + unsolved2
        s_unsolved += unsolved

        total1 = [x for x in smts if int(x['Instance']) == i]
        total2 = [x for x in milps if int(x['Instance']) == i]
        total = total1 + total2
        tot = float(len(total))

        X = [x for x in xrange(1)]

        u_data = [len(unsolved)/tot*100.0 for x in xrange(1)]
        smt_data = [len(solved_smts)/tot*100.0 for x in xrange(1)]
        usmt = [smt_data[i]+u_data[i] for i in xrange(1)]

        basic_sym = len(solved_basics_sym)/tot*100.0
        basic_asym = len(solved_basics_asym)/tot*100.0
        red_sym = len(solved_reduced_sym)/tot*100.0
        red_asym = len(solved_reduced_asym)/tot*100.0
        rel_sym = len(solved_relaxed_sym)/tot*100.0
        rel_asym = len(solved_relaxed_asym)/tot*100.0

        if plot:
            plot_instances(u_data, smt_data, basic_sym, basic_asym,
                           rel_sym, rel_asym, red_sym, red_asym, X,
                           usmt, inst)

        if debug:
            print "Instance: ", list(inst)[0]
            if (len(solved_smts) < len(solved_basics_sym) or
                len(solved_smts) < len(solved_basics_asym) or
                len(solved_smts) < len(solved_reduced_sym) or
                len(solved_smts) < len(solved_reduced_asym)or
                len(solved_smts) < len(solved_relaxed_sym) or
                len(solved_smts) < len(solved_relaxed_asym)):
                print "CHECK"
            print "solved_smts: ", len(solved_smts)
            print "solved_basics_sym: ", len(solved_basics_sym)
            print "solved_relaxed_sym: ", len(solved_relaxed_sym)
            print "solved_reduced_sym: ", len(solved_reduced_sym)
            print "solved_basics_asym: ", len(solved_basics_asym)
            print "solved_relaxed_asym: ", len(solved_relaxed_asym)
            print "solved_reduced_asym: ", len(solved_reduced_asym)
            print "unsolved: ", len(unsolved)
            print "total: ", len(total)
            print "--------------", "\n\n\n"

    return (s_smts, s_basics_sym, s_basics_asym,
            s_rel_sym, s_rel_asym, s_red_sym,
            s_red_asym, s_unsolved)


def plot(x, ys, title, ylabel, xlabel, xtick_labels):
    fig, ax = plt.subplots(len(x), sharex=True, sharey=True)
    width = 0.2
    # Generate some colors
    HSV_tuples = [(c*1.0/(len(ys.keys())), 0.3, 0.9)
                  for c in range(len(ys.keys()))]
    RGB_tuples = map(lambda r: colorsys.hsv_to_rgb(*r), HSV_tuples)

    xxtick_labels = ys.keys()
    for xx in x:
        for i, (k, v) in zip(range(len(ys.keys())), list(ys.iteritems())):
            ax[xx].bar(i*width, v[xx], color=RGB_tuples[i], width=width,
                       label=k)
            # ax[xx].set_xlabel(xtick_labels[xx])
            ax[xx].legend([xtick_labels[xx]], loc=0)

    ax[0].set_title(title)
    ax[len(x)-1].set_ylabel(ylabel)
    fig.set_size_inches(15, 25)
    fig.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)

    # plt.figlegend(ax, xtick_labels, bbox_to_anchor=(1.1, 1.07), loc=0)
    plt.xticks([(i*width) for i in xrange(len(ys.keys()))],
               xxtick_labels, rotation='vertical')
    # plt.minorticks_on()
    # plt.grid(which='major')
    # plt.grid(which='minor')
    # plt.show()
    plt.savefig(title+'.pdf', format='pdf', bbox_inches='tight')


def generate_csv_ccr(ccr, kw, csv_smt, csv_bs, csv_bas,
                     csv_rels, csv_relas, csv_reds, csv_redas):
    with open(kw+'.csv', 'w') as csvfile:
        fieldnames = ['CCR', 'SMT', 'ILP-BASIC', 'ILP-BASIC-ASYM',
                      'ILP-RELAXED', 'ILP-RELAXED-ASYM', 'ILP-REDUCED',
                      'ILP-REDUCED-ASYM']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({'CCR': 'CCR/Technique', 'SMT': 'SMT',
                         'ILP-BASIC': 'ILP-BASIC',
                         'ILP-BASIC-ASYM': 'ILP-BASIC-ASYM',
                         'ILP-RELAXED': 'ILP-RELAXED',
                         'ILP-RELAXED-ASYM': 'ILP-RELAXED-ASYM',
                         'ILP-REDUCED': 'ILP-REDUCED',
                         'ILP-REDUCED-ASYM': 'ILP-REDUCED-ASYM'})
        for s in ccr:
            smt = len([x for x in csv_smt if x['CCR'] == s])
            bs = len([x for x in csv_bs if x['CCR'] == s])
            bas = len([x for x in csv_bas if x['CCR'] == s])
            rel = len([x for x in csv_rels if x['CCR'] == s])
            rela = len([x for x in csv_relas if x['CCR'] == s])
            red = len([x for x in csv_reds if x['CCR'] == s])
            reds = len([x for x in csv_redas if x['CCR'] == s])
            writer.writerow({'CCR': s, 'SMT': str(smt),
                             'ILP-BASIC': str(bs),
                             'ILP-BASIC-ASYM': str(bas),
                             'ILP-RELAXED': str(rel),
                             'ILP-RELAXED-ASYM': str(rela),
                             'ILP-REDUCED': str(red),
                             'ILP-REDUCED-ASYM': str(reds)})


def generate_csv(structs, kw, csv_smt, csv_bs, csv_bas,
                 csv_rels, csv_relas, csv_reds, csv_redas):
    with open(str(kw)+'.csv', 'w') as csvfile:
        fieldnames = ['Structure', 'SMT', 'ILP-BASIC', 'ILP-BASIC-ASYM',
                      'ILP-RELAXED', 'ILP-RELAXED-ASYM', 'ILP-REDUCED',
                      'ILP-REDUCED-ASYM']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({'Structure': 'Structure/Technique', 'SMT': 'SMT',
                         'ILP-BASIC': 'ILP-BASIC',
                         'ILP-BASIC-ASYM': 'ILP-BASIC-ASYM',
                         'ILP-RELAXED': 'ILP-RELAXED',
                         'ILP-RELAXED-ASYM': 'ILP-RELAXED-ASYM',
                         'ILP-REDUCED': 'ILP-REDUCED',
                         'ILP-REDUCED-ASYM': 'ILP-REDUCED-ASYM'})
        for s in structs:
            smt = len([x for x in csv_smt if x['Structure'] == s])
            bs = len([x for x in csv_bs if x['Structure'] == s])
            bas = len([x for x in csv_bas if x['Structure'] == s])
            rel = len([x for x in csv_rels if x['Structure'] == s])
            rela = len([x for x in csv_relas if x['Structure'] == s])
            red = len([x for x in csv_reds if x['Structure'] == s])
            reds = len([x for x in csv_redas if x['Structure'] == s])
            writer.writerow({'Structure': s, 'SMT': str(smt),
                             'ILP-BASIC': str(bs),
                             'ILP-BASIC-ASYM': str(bas),
                             'ILP-RELAXED': str(rel),
                             'ILP-RELAXED-ASYM': str(rela),
                             'ILP-REDUCED': str(red),
                             'ILP-REDUCED-ASYM': str(reds)})


# Plot comparison of same structures from amongst the solved instances.
def plot_struct(s_smts, s_basics_sym, s_basics_asym,
                s_rel_sym, s_rel_asym, s_red_sym,
                s_red_asym, s_unsolved, kw, iterator,
                xlabel, structs):

    # ss_smt = []
    # us_smt = []
    # ss_b_s = []
    # us_b_s = []
    # ss_b_as = []
    # us_b_as = []
    # ss_rel_s = []
    # us_rel_s = []
    # ss_rel_as = []
    # us_rel_as = []
    # ss_red_s = []
    # us_red_s = []
    # ss_red_as = []
    # us_red_as = []

    if kw == 'Procs':
        for s in iterator:
            # This is for generating the csv files for the paper.
            csv_smt = [x for x in s_smts if x[kw] == s]
            csv_bs = [x for x in s_basics_sym if x[kw] == s]
            csv_bas = [x for x in s_basics_asym if x[kw] == s]
            csv_rels = [x for x in s_rel_sym if x[kw] == s]
            csv_relas = [x for x in s_rel_asym if x[kw] == s]
            csv_reds = [x for x in s_red_sym if x[kw] == s]
            csv_redas = [x for x in s_red_asym if x[kw] == s]
            generate_csv(structs, s, csv_smt, csv_bs, csv_bas,
                         csv_rels, csv_relas, csv_reds, csv_redas)
    elif kw == 'CCR':
        for s in structs:       # structs here really is just procs
            # This is for generating the csv files for the paper.
            csv_smt = [x for x in s_smts if x['Procs'] == s]
            csv_bs = [x for x in s_basics_sym if x['Procs'] == s]
            csv_bas = [x for x in s_basics_asym if x['Procs'] == s]
            csv_rels = [x for x in s_rel_sym if x['Procs'] == s]
            csv_relas = [x for x in s_rel_asym if x['Procs'] == s]
            csv_reds = [x for x in s_red_sym if x['Procs'] == s]
            csv_redas = [x for x in s_red_asym if x['Procs'] == s]
            generate_csv_ccr(iterator, str(s)+'_ccr', csv_smt, csv_bs, csv_bas,
                             csv_rels, csv_relas, csv_reds, csv_redas)

    # smt = len([x for x in s_smts if x[kw] == s])
    # ss_smt += [smt]
    # us_smt += [len([x
    #                 for x in s_unsolved
    #                 if x['Technique'] == 'smt' and x[kw] == s])]

    # b_sym = len([x for x in s_basics_sym if x[kw] == s])
    # ss_b_s += [b_sym]
    # us_b_s += [len([x
    #                 for x in s_unsolved
    #                 if x['Milptech'] == 'BASIC' and
    #                 x['Symmetry'] == 'no antisym' and x[kw] == s])]
    # b_asym = len([x for x in s_basics_asym if x[kw] == s])
    # ss_b_as += [b_asym]
    # us_b_as += [len([x
    #                  for x in s_unsolved
    #                  if x['Milptech'] == 'BASIC'and
    #                  x['Symmetry'] == 'antisym' and x[kw] == s])]

    # rel_sym = len([x for x in s_rel_sym if x[kw] == s])
    # ss_rel_s += [rel_sym]
    # us_rel_s += [len([x
    #                   for x in s_unsolved
    #                   if x['Milptech'] == 'RELAXED' and
    #                   x['Symmetry'] == 'no antisym' and x[kw] == s])]
    # rel_asym = len([x for x in s_rel_asym if x[kw] == s])
    # ss_rel_as += [rel_asym]
    # us_rel_as += [len([x
    #                    for x in s_unsolved
    #                    if x['Milptech'] == 'RELAXED' and
    #                    x['Symmetry'] == 'antisym' and x[kw] == s])]

    # red_sym = len([x for x in s_red_sym if x[kw] == s])
    # ss_red_s += [red_sym]
    # us_red_s += [len([x
    #                   for x in s_unsolved
    #                   if x['Milptech'] == 'REDUCED' and
    #                   x['Symmetry'] == 'no antisym' and x[kw] == s])]
    # red_asym = len([x for x in s_red_sym if x[kw] == s])
    # ss_red_as += [red_asym]
    # us_red_as += [len([x
    #                    for x in s_unsolved
    #                    if x['Milptech'] == 'REDUCED' and
    #                    x['Symmetry'] == 'antisym' and x[kw] == s])]

    # # The x-coordinates
    # X = [x for x in xrange(len(iterator))]
    # Y_smt = [x/(x+y)*100.0 for x, y in zip(ss_smt, us_smt)]
    # Y_b_s = [x/(x+y)*100.0 for x, y in zip(ss_b_s, us_b_s)]
    # Y_b_as = [x/(x+y)*100.0 for x, y in zip(ss_b_as, us_b_as)]
    # Y_rel_s = [x/(x+y)*100.0 for x, y in zip(ss_rel_s, us_rel_s)]
    # Y_rel_as = [x/(x+y)*100.0 for x, y in zip(ss_rel_as, us_rel_as)]
    # Y_red_s = [x/(x+y)*100.0 for x, y in zip(ss_red_s, us_red_s)]
    # Y_red_as = [x/(x+y)*100.0 for x, y in zip(ss_red_as, us_red_as)]

    # # The dictionary to plot
    # ys = {'smt': Y_smt, 'ilp_basic_sym': Y_b_s,
    #       'ilp_basic_asym': Y_b_as, 'ilp_relaxed_sym': Y_rel_s,
    #       ' ilp_relaxed_asym': Y_rel_as, 'ilp_reduced_sym': Y_red_s,
    #       'ilp_reduced_asym': Y_red_as}
    # plot(X, ys,
    #      'Comparison of different techniques for different' + xlabel,
    #      '% contribution of different techniques',
    #      xlabel, iterator)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    parser.add_argument("-t", "--task", help="Print the tasks chart",
                        action="store_true")
    parser.add_argument("-p", "--proc", help="Print the procs chart",
                        action="store_true")
    parser.add_argument("-c", "--ccr", help="Print the ccr chart",
                        action="store_true")
    parser.add_argument("-s", "--struct", help="Print the struct chart",
                        action="store_true")
    parser.add_argument("--plot", help="Print *all* the instance charts",
                        action="store_true")

    args = parser.parse_args()

    (smts, milps, largestInst,
     structs, ccrs, procs, tasks) = readFile(args.file)

    # Plotting the individual instances separately
    (s_smts, s_basics_sym, s_basics_asym,
     s_rel_sym, s_rel_asym, s_red_sym, s_red_asym,
     s_unsolved) = solved_instances(smts, milps, largestInst, args.plot,
                                    debug=False)

    if args.struct:
        plot_struct(s_smts, s_basics_sym, s_basics_asym,
                    s_rel_sym, s_rel_asym, s_red_sym, s_red_asym,
                    s_unsolved, 'Structure', structs, ' Task structures',
                    None)

    if args.ccr:
        plot_struct(s_smts, s_basics_sym, s_basics_asym,
                    s_rel_sym, s_rel_asym, s_red_sym, s_red_asym,
                    s_unsolved, 'CCR', ccrs, ' CCR', procs)

    if args.proc:
        plot_struct(s_smts, s_basics_sym, s_basics_asym,
                    s_rel_sym, s_rel_asym, s_red_sym, s_red_asym,
                    s_unsolved, 'Procs', procs, ' # of Procs', structs)

    if args.task:
        plot_struct(s_smts, s_basics_sym, s_basics_asym,
                    s_rel_sym, s_rel_asym, s_red_sym, s_red_asym,
                    s_unsolved, 'Tasks', tasks, ' # of Tasks', None)
