#!/usr/bin/env python

# import pydot
import os
# import gzip
# import shutil

from smt_formulate import calc_makespan_with_smt
from milp_formulate import calc_makespan_with_milp
import sys
# TEST_PROCS = [16]
TEST_PROCS = [2, 4, 8, 16]
MAX_TIME = 60
TEST_TYPES = ["BASIC", "RELAXED", "REDUCED"]


def parseFullname(fullname):

  fileChars = {}

  pathList = fullname.split('/')
  dir = pathList[2]
  filename = pathList[-1]
  if filename.startswith("InTree") or filename.startswith("OutTree"):
    cut = filename.find("_")
    fileLabel = filename[:cut]
    labelList = fileLabel.split('-')
    fileLabel = labelList[0]
    fileChars['spread'] = labelList[1]
    assert(labelList[2] == "MaxBf")
    fileChars['MaxBf'] = labelList[3]

  elif filename.startswith("SeriesParallel"):
    fileChars['spread'] = None
    cut = filename.find("_")
    fileLabel = filename[:cut]
    labelList = fileLabel.split('-')
    fileLabel = labelList[0]
    assert(labelList[1] == "MaxBf")
    fileChars['MaxBf'] = labelList[2]
  elif filename.startswith("Fork_Join"):
    cut = filename.find("Join_")
    cut += 4
    fileLabel = filename[:cut]
    fileChars['spread'] = None
    fileChars['MaxBf']  = None
  else: # Fork, Join, Independent, Random, Pipeline
    fileChars['spread'] = None
    fileChars['MaxBf']  = None
    if filename.startswith("Independent"):
      fileChars['CCR'] = None
    cut = filename.find("_")
    fileLabel = filename[:cut]

#  print "1:", fileLabel
  charList = filename[(cut+1):-4].split('_')
  fileChars.update(dict([(charList[2*n], charList[2*n+1]) for n in range(len(charList) / 2)]))
#  print "2:", fileChars

  return dir, fileLabel, fileChars

def writeResult(label, chars, procs, alg, var1, var2, l, u, t):

  strList = ["RESULT", label, chars['spread'], chars['MaxBf'], chars['Nodes'], chars['CCR'], chars['WeightType'],
             procs, alg, var1, var2, l, u, t]

  print ','.join(str(x) for x in strList)

USE_FULL_DIR = False

if not USE_FULL_DIR:
  sys.path.append(".")
  from batch_files import my_files, get_done, is_done
  get_done()

# types = ["Join", "Random", "Fork", "Fork Join", "Stencil",
#          "Pipeline", "In Tree", "Out Tree", "Series Parallel"]
types =  ["Independent"]
# types =  ["Stencil"]
# CCR = ["CCR 0.1", "CCR 1.0", "CCR 10.0"]
# CCR = ["CCR 0.1"]
for tt in types:
  # for ccr in CCR:
    for procs in TEST_PROCS:
      if tt == 'Independent':
        ccr = None
      for filename in my_files(tt, ccr):
        fullname = filename
        print "Processing", fullname
        dir, label, chars = parseFullname(fullname)
        # SMT
        lb, ub, takes = calc_makespan_with_smt(fullname, 
                                               procs,
                                               maxtime=MAX_TIME,
                                               verbose=False,
                                               progress=True, timed=True)
        writeResult(label, chars, procs, "smt", None, None, lb, ub, takes)
        # ILP
        for type in TEST_TYPES:
            for antisym in [False, True]:
                makespan, takes = calc_makespan_with_milp(fullname, 
                                                          procs,
                                                          maxtime=MAX_TIME,
                                                          type=type,
                                                          verbose=False,
                                                          progress=True,
                                                          timed=True,
                                                          antisymmetry=antisym)
                # os.system('kinit -R ; aklog')
                if makespan is None:
                  makespan = "Check MILP trace"
                if antisym:
                  writeResult(label, chars, procs, "milp", type, 
                              "antisym", makespan, makespan, takes)
                else:
                  writeResult(label, chars, procs, "milp", 
                              type, "no antisym", makespan, makespan, takes)
        # os.system('kinit -R ; aklog')

    # Carry out parsing of the results for this benchmark for all the processors
    print "Done", tt, ccr
    # with open('./graphs/output.txt', 'rb') as f_in,
    # gzip.open('output_'+tt+'_'+ccr+'.txt.gz', 'wb') as f_out:
    #   shutil.copyfileobj(f_in, f_out)
