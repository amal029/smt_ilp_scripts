import pydot
import pygtk
import gtk
import xdot

root = '/home/mosu001/orua/orict/cloud-and-storage/parallel-lab/'
#filename = root + "proc_sched/data/207_one_minute_exp_graphs/Random_Nodes_10_Density_0.20_CCR_1.00_WeightType_Random"
filename = root + "proc_sched/data/DAG4/Fork Join/Nodes 30/CCR 10.0/Random/Fork_Join_Nodes_30_CCR_9.94_WeightType_Random"

graph = pydot.graph_from_dot_file(filename + ".dot")

print graph.to_string()

#graph.write_png(filename + ".png")
window = xdot.DotWindow()
window.set_dotcode(graph.to_string())
window.connect('destroy', gtk.main_quit)
gtk.main()



