#!/usr/bin/env python

#files = ["graphs/output.txt"]
files = ["graphs/output.txt", "graphs/Independent.txt"]

seen = {}

def findOccurences(s, ch):
    return [i for i, letter in enumerate(s) if letter == ch]

count = 0
for filename in files:
  f = open(filename, 'r')
  parsing = False
  for r in f:
    if r.startswith("Processing"):
      # print 'r: ', r
      # Get DAG directory
      slashes = findOccurences(r, "/")
      dagLU = slashes[1:3]
#      print dagLU
      dagDir = r[(dagLU[0]+1):dagLU[1]]
      # print 'dagDIR: ', dagDir
      # Get test name
      fileL = slashes[-1]
      testName = r[(fileL+1):]
      # print 'testName: ', testName
      if (dagDir, testName) not in seen.keys():
        seen[dagDir, testName] = {}
      parsing = True
      objStr = "NA"
      bndStr = "NA"

    if parsing:
      if r.startswith("Best objective"):
        commas = findOccurences(r, ',')
        objStr = r[:commas[0]]
        objStr = objStr.replace("Best objective ", "")
        bndStr = r[(commas[0] + 1):commas[1]]
        bndStr = bndStr.replace(" best bound ", "")
      elif r.startswith("RESULT"):
#        print r
        s = r.rstrip().replace("RESULT,", "")
        srcStr = "Check MILP trace"
        check = s.find(srcStr)
        if check != -1:
          # Replace srcStr with bndStr
          s = s[:check] + bndStr + s[(check + len(srcStr)):]
          check = s.find(srcStr)
          if check != -1:
            # Replace srcStr with objStr
            s = s[:check] + objStr + s[(check + len(srcStr)):]
        
        # print 'trying to add s: ', s
        cols = s.split(',')
        procs = cols[6]
        alg = cols[7]
        subalg = cols[8]
        extra = cols[9]
        
        # if (procs, alg, subalg, extra) not in seen[dagDir, testName].keys():
        count += 1
        seen[dagDir, testName][procs, alg, subalg, extra, count] = s
  f.close()
  
print ''.join(str(k[0]) + " " + str(k[1]) for k in seen.keys())

import csv

with open('seen.csv', 'wb') as csvfile:
    seenwriter = csv.writer(csvfile, delimiter=',')
    for k in seen.keys():
      seenwriter.writerow([str(k[0]), str(k[1]).strip()]) 

count = 0
with open('results.csv', 'wb') as csvfile:
    reswriter = csv.writer(csvfile, delimiter=',')
    for k in seen.keys():
      for l in seen[k].keys():
        count += 1
        # print "Count =", count
#        print r
        row = seen[k][l].split(',')
        print row
        row.insert(0, k[0])
        reswriter.writerow(row) 
