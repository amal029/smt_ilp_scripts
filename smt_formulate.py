import pydot_ng as pydot
#import pygtk
#import gtk
#import xdot

# root = "/home/mosu001/orua/orict/cloud-and-storage/parallel-lab"
import sys
# sys.path.append(root + "/Z3/build")
sys.path.append("/opt/z3/lib/python2.7/dist-packages")
from z3 import *

import networkx as nx

from math import ceil

import time

def calc_makespan_with_smt(filename, num_procs, maxtime = 60,
                           verbose = False, progress = True, timed = False):

  if timed: t0 = time.time()

  graph = pydot.graph_from_dot_file(filename)
#  print graph.to_string()

  #if verbose:
    #window = xdot.DotWindow()
    #window.set_dotcode(graph.to_string())
    #window.connect('destroy', gtk.main_quit)
    #gtk.main()

  nodes = [v for v in graph.get_nodes() if v.get_name() != "graph"]
  edges = graph.get_edges()

  procs = range(num_procs)

  if verbose:
    print "Nodes =", [v.get_name() for v in nodes]
    print "Procs =", procs

  b = dict([((v.get_name(), p), Bool("b_%s_%s" % (v.get_name(), p))) for v in nodes for p in procs])
  if verbose: print b

  s = dict([(v.get_name(), Real("s_%s" % v.get_name())) for v in nodes])
  if verbose: print s

  t = dict([(v.get_name(), float(v.get_attributes()['Weight'])) for v in nodes])
  if verbose: print t

  c = dict([((e.get_source(), e.get_destination()), float(e.get_attributes()['Weight'])) for e in edges])
  if verbose: print c

  if verbose: sys.stdout.flush()

  G = nx.DiGraph(nx.read_dot(filename))
  no_path = []
  for i1 in range(len(nodes)):
    for i2 in range(i1 + 1, len(nodes)):
      v1 = nodes[i1].get_name()
      v2 = nodes[i2].get_name()
      if nx.has_path(G, v1, v2) or nx.has_path(G, v2, v1):
        continue
      no_path.append((v1, v2))
  if verbose: print "No paths =", no_path

#  print graph.to_string()
#  print [v.get_name() for v in graph.get_nodes()]
#  print graph.get_node('graph')
  ub = graph.get_node('graph')[0].get_attributes()['"Total sequential time"']
  if verbose: print ub
  calc_ub = sum(t[v.get_name()] for v in nodes)
  if verbose: print calc_ub
  calc_lb = calc_ub / len(procs)
  if verbose: print calc_lb

  ub = calc_ub
  lb = calc_lb

  Max = Real("Max")
  Min = Real("Min")

  solver = Solver()
  
  # Define Max
  if verbose: print "Defining Max"
  for v in nodes:
    solver.add(Max >= s[v.get_name()] + t[v.get_name()])

  # Define Min
  if verbose: print "Defining Min"
  for v in nodes:
    solver.add(Min <= s[v.get_name()])

  # (A)
  if verbose: print "Defining (A)"
  for v in nodes:
    solver.add(Or([b[v.get_name(), p] for p in procs]))

  # (C1)
  if verbose: print "Defining (C1)"
  for e in edges:
    v1 = e.get_source()
    v2 = e.get_destination()
    for p1 in procs:
      for p2 in procs:
        if p1 != p2:
          solver.add(Implies(And(b[v1, p1], b[v2, p2]), s[v2] >= s[v1] + t[v1] + c[v1, v2]))

  # (C2)
  if verbose: print "Defining (C2)"
  for e in edges:
    v1 = e.get_source()
    v2 = e.get_destination()
    for p in procs:
      solver.add(Implies(And(b[v1, p], b[v2, p]), s[v2] >= s[v1] + t[v1]))

  # (R)
  if verbose: print "Defining (R)"
  for (v1, v2) in no_path:
    for p in procs:
      solver.add(Implies(And(b[v1, p], b[v2, p]),
                         Or(s[v1] >= s[v2] + t[v2], s[v2] >= s[v1] + t[v1])))

#b_0_0 = False
#b_0_1 = True
#b_1_0 = False
#b_1_1 = True
#b_2_0 = False
#b_2_1 = True
#b_3_0 = True
#b_3_1 = None
#b_4_0 = False
#b_4_1 = True
#b_5_0 = False
#b_5_1 = True
#b_6_0 = False
#b_6_1 = True
#b_7_0 = False
#b_7_1 = True
#b_8_0 = False
#b_8_1 = True
#b_9_0 = False
#b_9_1 = True
#s_0 = -45
#s_1 = -20
#s_2 = -13
#s_3 = -27
#s_4 = -14
#s_5 = -16
#s_6 = -16
#s_7 = -16
#s_9 = -7
#s_8 = -16

  if verbose: print "Solving..."

#  Mike: Removed lb solve as it is hard (takes a long time),
#  will most likley be "unsat"and, if not, will be identified pretty quickly
#  using binary search anyway
#  solver.push()
#  solver.add(Max - Min <= lb)
#  status = solver.check()
#  if verbose: print "lb =", lb, "lb status =", status, "check", status == CheckSatResult(Z3_L_TRUE)
#  if status == CheckSatResult(Z3_L_TRUE):
#    ub = lb
#  else:
#    solver.pop()
#    solver.push()
#    solver.add(Max - Min <= ub)
#    status = solver.check()
#    if verbose: print "ub =", ub, "ub status =", status, "check", status == CheckSatResult(Z3_L_TRUE)
#    if status != CheckSatResult(Z3_L_TRUE):
#      print "Error: problem could not be satisfied..."
#      return None

  solver.push()
  solver.add(Max - Min <= ub)
  
  if timed:
      time_left = maxtime
      print "Solve time remaining =", time_left, "s, timeout =", int(ceil(time_left)) * 1000
      solver.set("timeout", int(ceil(time_left)) * 1000)

  status = solver.check()
  if verbose: print "ub =", ub, "ub status =", status, "check", status == CheckSatResult(Z3_L_TRUE)
  if status != CheckSatResult(Z3_L_TRUE):
    print "Error: problem could not be satisfied..."
    if timed:
        t1 = time.time()
        return lb, float("inf"), t1 - t0
    else:
        return lb, float("inf"), None        

  tol = 1e-6
  if timed:
    t1 = time.time()
    time_left = maxtime - (t1 - t0)
  if progress:
    print "[", lb, ",", ub, "]"
    if timed:
      print "calc time =", t1 - t0
    sys.stdout.flush()

  while lb + tol < ub:
    mid = (lb + ub) / 2
    solver.pop()
    solver.push()
    solver.add(Max - Min <= mid)
  
    if timed:
        print "Solve time remaining =", time_left, "s, timeout =", int(ceil(time_left)) * 1000
        solver.set("timeout", int(ceil(time_left)) * 1000)

    status = solver.check()
    if verbose: print "mid =", mid, "mid status =", status, "check", status == CheckSatResult(Z3_L_TRUE)
    if status == CheckSatResult(Z3_L_TRUE):
      ub = mid
    elif status == CheckSatResult(Z3_L_FALSE):
      lb = mid
    else:
      print "Could not solve, quitting..."
      t1 = time.time()
      if timed: return lb, ub, t1 - t0
      else:     return lb, ub, None
    if timed:
      t1 = time.time()
      time_left = maxtime - (t1 - t0)
    if progress:
      print "[", lb, ",", ub, "]"
      if timed:
        t1 = time.time()
        print "calc time =", t1 - t0
      sys.stdout.flush()

  if timed: t1 = time.time()

  if verbose:
    solver.pop()
    solver.push()
    solver.add(Max - Min <= ub)
    solver.check()
    print "lb =", lb, "& ub =", ub
    m = solver.model()
    for bv in b.values():
      print bv, "=", m[bv]
    for sv in s.values():
      print sv, "=", m[sv]
  
    print [m[sv].as_decimal(5)[0:-1] for sv in s.values()]
    earliest = min(float(m[sv].as_decimal(5)[0:-1]) for sv in s.values())
    for sv in s.values():
      print sv, "=", float(m[sv].as_decimal(5)[0:-1]) - earliest

  if timed: return lb, ub, t1 - t0

  return lb, ub, None

#x = Real('x')
#y = Real('y')
#s = Solver()
#s.add(x + y > 5, x > 1, y > 1)
#print(s.check())
#print(s.model())

if __name__ == '__main__':

  filename = root + "/proc_sched/data/207_one_minute_exp_graphs/OutTree-Unbalanced-MaxBf-3_Nodes_10_CCR_1.96_WeightType_Random.dot"

  makespan, takes = calc_makespan_with_smt(filename, 2, verbose = True)

  print "Makespan =", makespan, "Takes =", takes

