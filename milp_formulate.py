import pydot_ng as pydot
import pygtk
import gtk
#import xdot

#root = "/home/mosu001/orua/orict/cloud-and-storage/parallel-lab"
#root = "/home/mike/smt-ilp/graphs"
root = "D:\docs\orua\orict\cloud-and-storage\parallel-lab\smt-ilp\graphs"

import sys

from coinor.pulp import *

try:
    
  import coinor.dippy as dippy
  DIPPY_INSTALLED = True
  
except ImportError:
  DIPPY_INSTALLED = False

try:
    
  import gurobipy
  GUROBI_INSTALLED = True
  
except ImportError:
  GUROBI_INSTALLED = False

GUROBI_INSTALLED = False

print DIPPY_INSTALLED, GUROBI_INSTALLED

import networkx as nx

import time

def calc_makespan_with_milp(filename, num_procs, maxtime = 60, type = "BASIC",
                            verbose = False, progress = True, timed = False,
                            antisymmetry = False):

  if timed: t0 = time.time()

  graph = pydot.graph_from_dot_file(filename)
#  print graph.to_string()

  #if verbose:
    #window = xdot.DotWindow()
    #window.set_dotcode(graph.to_string())
    #window.connect('destroy', gtk.main_quit)
    #gtk.main()

  if DIPPY_INSTALLED:
    prob = dippy.DipProblem("proc_sched",
                            display_mode = 'xdot',
#                            display_interval = None)
                            display_interval = 100)
  else:
    prob = LpProblem("proc_sched", LpMinimize)   
  
  prob.tol = pow(pow(2, -24), 2.0 / 3.0)

  nodes = [v for v in graph.get_nodes() if v.get_name() != "graph"]
  edges = graph.get_edges()

  procs = range(num_procs)

  if verbose:
    print "Nodes =", [v.get_name() for v in nodes]
    print "Procs =", procs

  NODES = [i.get_name() for i in nodes]
  PAIRS = [(i.get_name(), j.get_name()) for i in nodes for j in nodes]
  PREDS = dict([(i.get_name(), []) for i in nodes])
  for e in edges:
    PREDS[e.get_destination()].append(e.get_source())
  if verbose: print PREDS

  sigma = LpVariable.dicts("sigma", PAIRS, cat=LpBinary)
  if verbose: print sigma

  eps = LpVariable.dicts("epsilon", PAIRS, cat=LpBinary)
  if verbose: print eps
  
  t = dict([(v.get_name(), float(v.get_attributes()['Weight'])) for v in nodes])
  if verbose: print t

  if type == "NEW":
    x = LpVariable.dicts("x", [(i, j) for i in NODES for j in procs], cat=LpBinary)
    p = {}
    for i in NODES: p[i] = lpSum(j * x[i, j] for j in procs)
    if antisymmetry:
      for j in procs:
        if j > 0:
#          prob += lpSum(x[i, j - 1] for i in NODES) >= lpSum(x[i, j] for i in NODES)
          prob += lpSum(t[i] * x[i, j - 1] for i in NODES) >= lpSum(t[i] * x[i, j] for i in NODES)
  else:
    p = LpVariable.dicts("p", NODES, 0, len(procs), LpInteger)
  if verbose: print p

  s = LpVariable.dicts("s", NODES, 0, None)
  if verbose: print s

  W = LpVariable("W", 0, None)
  if verbose: print W
  
  c = dict([((e.get_source(), e.get_destination()), float(e.get_attributes()['Weight'])) for e in edges])
  if verbose: print c

  if verbose: sys.stdout.flush()

  G = nx.DiGraph(nx.read_dot(filename))
  no_path = []
  for i1 in range(len(nodes)):
    for i2 in range(i1 + 1, len(nodes)):
      v1 = nodes[i1].get_name()
      v2 = nodes[i2].get_name()
      if nx.has_path(G, v1, v2) or nx.has_path(G, v2, v1):
        continue
      no_path.append((v1, v2))
  if verbose: print "No paths =", no_path

#  print graph.to_string()
#  print [v.get_name() for v in graph.get_nodes()]
#  print graph.get_node('graph')
  ub = graph.get_node('graph')[0].get_attributes()['"Total sequential time"']
  if verbose: print ub
  calc_ub = sum(t[v.get_name()] for v in nodes)
  if verbose: print calc_ub
  calc_lb = calc_ub / len(procs)
  if verbose: print calc_lb

  ub = calc_ub
  lb = calc_lb

  Wmax = sum(t[i] for i in NODES) + sum(c[e.get_source(), e.get_destination()] for e in edges)
  
#  if (not antisymmetry) or (antisymmetry and type == 'NEW'):
  if not antisymmetry:
    prob += W # Minimise W (A01)
  else:
    max_perturb = sum(n * len(procs) for n in range(len(NODES)))
    perturb_inc = lb / max_perturb / 10
    perturb = dict([(i, n * perturb_inc) for n, i in enumerate(NODES)])
    if verbose: print perturb
    prob += W + lpSum(perturb[i] * p[i] for i in NODES) # Minimise W (A01)
    if verbose: print W + lpSum(perturb[i] * p[i] for i in NODES)
  
  for i in NODES:
      prob += s[i] + t[i] <= W # (A02)
      
  for (i, j) in PAIRS:
    if i != j:
      if type != "REDUCED":
        prob += sigma[i, j] + sigma[j, i] <= 1 # (A03)
        prob += eps[i, j] + eps[j, i] <= 1 # (A04)
      prob += sigma[i, j] + sigma[j, i] + eps[i, j] + eps[j, i] >= 1 # (A05)
      prob += p[j] - p[i] - 1 - (eps[i, j] - 1) * len(procs) >= 0 # (A06)
      if type == "BASIC":
          prob += p[j] - p[i] - eps[i, j] * len(procs) <= 0 # (A07)
      prob += s[i] + t[i] + (sigma[i, j] - 1) * Wmax <= s[j] # (A08)
  
  for j in NODES:
      for i in PREDS[j]:
          if (type == "RELAXED") or (type == "REDUCED"):
              prob += p[j] - p[i] - eps[i, j] * len(procs) <= 0 # (A07a)
              prob += p[i] - p[j] - eps[j, i] * len(procs) <= 0 # (A07b)
          prob += s[i] + t[i] + c[i, j]  * (eps[i, j] + eps[j, i]) <= s[j] # (A09)
          prob += sigma[i, j] == 1 # (A10)

  if verbose: print "Solving..."

  tol = 1e-6

  prob.writeLP("milp.lp")
  if GUROBI_INSTALLED:
      print "Solve using Gurobi..."
#      prob.solve(GUROBI(timeLimit=maxtime))
#      options = [('TimeLimit', maxtime)]
      options = []
      prob.solve(GUROBI_CMD(options=options))
      status = prob.status
  elif DIPPY_INSTALLED:
      prob.x = x
      prob.branch_method = priority_branch
      print "Solve using Dippy..."
      status, message, sol, duals = dippy.Solve(prob)
  else:
      print "Solve using Cbc.."
      prob.solve(PULP_CBC_CMD(msg=1))
      status = prob.status

  if verbose:
      for i in NODES:
          print "Start node", i, "on", p[i].varValue, "at", s[i].varValue

  if timed:
      t1 = time.time()
      if status == LpStatusOptimal:
          return W.varValue, t1 - t0
      else:
          return None, t1 - t0

  if status == LpStatusOptimal:
      return W.varValue, None
  else:
      return None, None

from math import floor, ceil

def branch_on_xs(prob, sol):
  fracVar = None
  mostFrac = prob.tol
  
  if fracVar is None:
      x_vars = prob.x
  
      for v in x_vars.values():
          print v.name, "=", sol[v]
          frac = min(ceil(sol[v]) - sol[v], sol[v] - floor(sol[v]))
          if frac > mostFrac:
              # Integer/Binary variable that is fractional
              fracVar = v
              mostFrac = frac
                                    
  return fracVar, mostFrac

def priority_branch(prob, sol):
  
  bounds = None
  
  fracVar = None
  mostFrac = prob.tol
  
  if fracVar is None:
      fracVar, mostFrac = branch_on_xs(prob, sol)

  if fracVar is not None:
      down_lbs = {}
      down_ubs = {}
      up_lbs   = {}
      up_ubs   = {}
      
      down_ubs[fracVar] = floor(sol[fracVar])
      up_lbs[fracVar]   = ceil(sol[fracVar])
      
      return down_lbs, down_ubs, up_lbs, up_ubs
                  
  return bounds

if __name__ == '__main__':

#  filename = root + "/proc_sched/data/207_one_minute_exp_graphs/OutTree-Unbalanced-MaxBf-3_Nodes_10_CCR_1.96_WeightType_Random.dot"
#  filename = root + "/DAG1/Fork/Nodes 10/CCR 0.1/Random/Fork_Nodes_10_CCR_0.10_WeightType_Random.dot"
  filename = root + "\DAG1\Fork\Nodes 10\CCR 0.1\Random\Fork_Nodes_10_CCR_0.10_WeightType_Random.dot"
#  filename = root + "/ogra20_60.dot"
#  filename = root + "\ogra20_60.dot"

#  makespan, takes = calc_makespan_with_milp(filename, 2, verbose = True)#, antisymmetry = True)
  makespan, takes = calc_makespan_with_milp(filename, 2, verbose = True, type = "NEW", antisymmetry = True)
#  makespan, takes = calc_makespan_with_milp(filename, 2, verbose = False, timed = True)

  print "Makespan =", makespan, "Takes =", takes
  
